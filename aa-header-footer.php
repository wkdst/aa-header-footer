<?php
/*
Plugin Name: AA Header and Footer
Description:  Plugin for maintaining AA Header and Footer across multiple sites
Version:      1.0.0
License:      GPL2
License URI:  https://www.gnu.org/licenses/gpl-2.0.html
Text Domain:  aa-header-footer
*/

require_once plugin_dir_path( __FILE__ ) . 'includes/scripts-widgets.php';

// Add header 
function aahf_header() {
	ob_start();
	?>
	<header class="site-header">
	  <div class="site-header__top">
	    <div class="container">
        <div class="row">
          <div class="nav-top dib fr vat">
            <div class="dib tar">
    	    	<?php if (has_nav_menu('aa_top_navigation')) {
    	    		wp_nav_menu(['theme_location' => 'aa_top_navigation', 'menu_class' => 'nav dib sf-menu ttu', 'container' => '']);
    	    	} ?>
            </div>
            <div class="dib vat">
              <a href="https://www.findapprenticeship.service.gov.uk/apprenticeshipsearch" target="blank" class="site-header__top--apprenticeship-logo" title="Find an Apprenticeship"><img src="<?php echo plugin_dir_url( __FILE__ ) . 'assets/images/apprenticeships-logo.png'; ?>" alt="Find an Apprenticeship"></a>
              <a href="#" class="btn white bg-blue ttu fs10 ls1">Newsletter</a>
              <a href="#" class="btn white bg-orange ttu fs10 ls1">Login</a>
            </div>
          </div>
        </div>
	    </div>
	  </div>
	  <div class="site-header__main">
	    <div class="container">
        <div class="row">
  	      <a  href="<?php echo home_url('/'); ?>" class="site-brand fl">
            <div class="site-header__main--aa-logo">
            <img src="<?php echo plugin_dir_url( __FILE__ ) . 'assets/images/amazing-apprenticeships.png'; ?>" title="<?php echo get_bloginfo('name', 'display'); ?>" alt="<?php echo get_bloginfo('name', 'display'); ?>">
            </div>
          </a>
  	      <nav class="nav-primary tar">
  	      	<?php if (has_nav_menu('aa_primary_navigation')) {
  				wp_nav_menu(['theme_location' => 'aa_primary_navigation', 'menu_class' => 'nav dib sf-menu fh ttu', 'container' => '']);
  	      	} ?>
            <div class="block-social dib tar vat">
              <a href="https://twitter.com/amazingappsuk" target="_blank"><span class="icon-twitter"></span></a>
              <a href="https://www.instagram.com/amazingappsuk" target="_blank"><span class="icon-instagram"></span></a>
              <a href="https://www.linkedin.com/groups/8357148/profile" target="_blank"><span class="icon-linkedin"></span></a>
            </div>
  	      </nav>

        </div>
      	</div>
	  </div>
	</header>	<?php
	echo ob_get_clean();
}
add_action( 'get_header', 'aahf_header' );

// Add header 
function aahf_footer() {
  ob_start();
  ?>
  <footer class="site-footer bg-black pv50">
      <div class="container">
          <div class="row">
              <div class="col-sm-3">
                  <img src="<?php echo plugin_dir_url( __FILE__ ) . 'assets/images/logo_footer.png'; ?>" alt="Amazing Apprenticeships">
                  <p class="white">Amazing Apprenticeships is the approved communication channel, commissioned by the National Apprenticeship Service, to support the Apprenticeship Support & Knowledge for Schools project.</p>
                  <?php dynamic_sidebar('aa_sidebar-footer-1'); ?>
              </div>
              <div class="col-sm-3">
                  <h3 class="pink ttu">Stay Connected</h3>
                  <a href="#" class="btn white bg-orange ttu fs13 ls1 db mb10 tal">Request Support</a>
                  <a href="#" class="btn white bg-orange ttu fs13 ls1 db mb10 tal">Staff Login</a>
                  <a href="#" class="btn white bg-orange ttu fs13 ls1 db mb10 tal">Champion Login</a>
                  <a href="#" class="btn white bg-orange ttu fs13 ls1 db mb10 tal">Newsletter</a>
                  <?php dynamic_sidebar('aa_sidebar-footer-2'); ?>
              </div>
              <div class="col-sm-3 fh">
                  <h3 class="mint ttu">Main Pages</h3>
                  <?php dynamic_sidebar('aa_sidebar-footer-3'); ?>
              </div>
              <div class="col-sm-3">
                  <h3 class="blue ttu">Latest Tweets</h3>
                  <div class="footer-tweets"><div id="tweets"></div><div id="tweets-follow"></div></div>
                  <script>(function($){$('#tweets').twittie({username:'AmazingAppsUK',template:'<p class="twit-content">{{tweet}}</p>',count:3,hideReplies:!0,apiPath:'<?php echo plugin_dir_url( __FILE__ ) . 'tweetie/tweet.php'; ?>',});$('#tweets-follow').twittie({username:'AmazingAppsUK',template:'<a href="https://twitter.com/{{user_name}}" target="_blank"><span class="icon-twitter"></span> Follow Us</a>',count:1,apiPath:'<?php echo plugin_dir_url( __FILE__ ) . 'tweetie/tweet.php'; ?>',})})(jQuery)</script>
                  <?php dynamic_sidebar('aa_sidebar-footer-4'); ?>
              </div>
          </div>
      </div>
  </footer>
  <?php
  echo ob_get_clean();
}
add_action( 'get_footer', 'aahf_footer' );

// Add footer bottom 
function aahf_footer_bottom() {
  ob_start();
  ?>
  <div class="footer__bottom bg-blue">
    <div class="container pv25">
      <div class="row">
      copy right
      </div>
    </div>
  </div>
  <?php
  echo ob_get_clean();
}
add_action( 'get_footer', 'aahf_footer_bottom', 100 );


