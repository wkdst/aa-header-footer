<?php 
function aahf_add_scripts() {   
    wp_enqueue_style( 'aa-hf-css', plugin_dir_url( __FILE__ ) . '../assets/css/aa-hf.css' );
    wp_enqueue_script( 'aa-hf-js', plugin_dir_url( __FILE__ ) . '../assets/js/aa-hf.js', array( 'jquery' ) );
}
add_action('wp_enqueue_scripts', 'aahf_add_scripts');

// remove wp version param from any enqueued scripts
function vc_remove_wp_ver_css_js( $src ) {
    if ( strpos( $src, 'ver=' . get_bloginfo( 'version' ) ) )
        $src = remove_query_arg( 'ver', $src );
    return $src;
}
add_filter( 'style_loader_src', 'vc_remove_wp_ver_css_js', 9999 );
add_filter( 'script_loader_src', 'vc_remove_wp_ver_css_js', 9999 );

function aahf_add_menus_widgets() {
	// Menus
    register_nav_menus([
        'aa_top_navigation' => __('Top Navigation', 'aa-header-footer'),
        'aa_primary_navigation' => __('Primary Navigation', 'aa-header-footer'),
    ]);
    // Footer Widgets
	$config = [
		'before_widget' => '<div class="widget %1$s %2$s">',
		'after_widget' => '</div>',
		'before_title' => '<h3 class="widget__title">',
		'after_title' => '</h3>'
	];
    register_sidebar([
            'name' => __('Primary', 'aa-header-footer'),
            'id' => 'sidebar-primary'
        ] + $config);
    register_sidebar([
            'name' => __('Footer 1', 'aa-header-footer'),
            'id' => 'aa_sidebar-footer-1'
        ] + $config);
    register_sidebar([
            'name' => __('Footer 2', 'aa-header-footer'),
            'id' => 'aa_sidebar-footer-2'
        ] + $config);
    register_sidebar([
            'name' => __('Footer 3', 'aa-header-footer'),
            'id' => 'aa_sidebar-footer-3'
        ] + $config);
    register_sidebar([
            'name' => __('Footer 4', 'aa-header-footer'),
            'id' => 'aa_sidebar-footer-4'
        ] + $config);
}
add_action( 'init', 'aahf_add_menus_widgets' );
